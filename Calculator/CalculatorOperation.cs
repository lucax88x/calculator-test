namespace Calculator
{
    public enum CalculatorOperation
    {
        Sum = 0,
        Diff = 1,
        Div = 2,
        Mult = 3
    }
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class Program
    {                
        public static void Main(string[] args)
        {
            var programLogic = new ProgramLogic(new ConsoleWrapper(), new FileWrapper());
            
            programLogic.Run(args);
        }       
    }
}
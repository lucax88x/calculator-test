using System;

namespace Calculator
{
    public interface IConsoleWrapper
    {
        void WriteLine(object line);
        string ReadLine();
    }

    public class ConsoleWrapper : IConsoleWrapper
    {
        public void WriteLine(object line)
        {
            Console.WriteLine(line.ToString());
        }        
        
        public string ReadLine()
        {
            return Console.ReadLine();
        }
    }
}
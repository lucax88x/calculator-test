namespace Calculator
{
    internal class CalculatorParameters
    {
        private readonly CalculatorOperation _operation;
        private readonly long _operand1;
        private readonly long _operand2;

        public CalculatorParameters(CalculatorOperation operation, long operand1, long operand2)
        {
            _operation = operation;
            _operand1 = operand1;
            _operand2 = operand2;
        }

        public long Compute()
        {
            switch (_operation)
            {
                case CalculatorOperation.Diff:
                    return _operand1 - _operand2;
                case CalculatorOperation.Div:
                    return _operand1 / _operand2;
                case CalculatorOperation.Mult:
                    return _operand1 * _operand2;
                default:
                    return _operand1 + _operand2;
            }
        }
    }
}
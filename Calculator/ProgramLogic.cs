using System;
using System.Linq;

namespace Calculator
{
    public class ProgramLogic
    {
        private readonly IConsoleWrapper _console;
        private readonly IFileWrapper _file;

        public ProgramLogic(IConsoleWrapper console, IFileWrapper file)
        {
            _console = console;
            _file = file;
        }

        public void Run(string[] args)
        {
            try
            {
                var parameters = ReadParameters(args);
                var calculatorParameters = ParseParameters(parameters);
                var result = calculatorParameters.Compute();
                _console.WriteLine(result);
            }
            catch (FormatException ex)
            {
                _console.WriteLine(ex.Message);
            }
        }

        private (string argument1, string operation, string argument2) ReadParameters(string[] args)
        {
            if (args.Length == 0)
            {
                throw new FormatException("you must provide an argument (interactive or a filepath)");
            }

            var arg = args.First();
            return IsInteractive(arg) ? ReadAsInteractive() : ReadAsNormal(arg);
        }

        private static bool IsInteractive(string arg)
        {
            return arg.ToLower() == "interactive";
        }

        private (string argument1, string operation, string argument2) ReadAsInteractive()
        {
            _console.WriteLine("First argument: ");
            var argument1 = _console.ReadLine();

            _console.WriteLine("Operation (+, -, /, *): ");
            var operation = _console.ReadLine();

            _console.WriteLine("Second Argument: ");
            var argument2 = _console.ReadLine();

            return (argument1, operation, argument2);
        }

        private (string operation, string argument1, string argument2) ReadAsNormal(string arg)
        {
            var readAllText = _file.ReadAllText(arg);

            var strings = readAllText.Split("\r\n".ToCharArray());

            if (strings.Length < 3)
            {
                throw new FormatException("You must provide 2 operands separated by a operation (+, -, /, *)");
            }

            var argument1 = strings[0];
            var operation = strings[1];
            var argument2 = strings[2];

            return (argument1, operation, argument2);
        }

        private CalculatorParameters ParseParameters((string argument1, string operation, string argument2) args)
        {
            if (!TryParseOperation(args.operation, out var operation))
            {
                throw new FormatException("operation has wrong format");
            }

            if (!long.TryParse(args.argument1, out var firstOperand))
            {
                throw new FormatException("first operand has wrong format");
            }

            if (!long.TryParse(args.argument2, out var secondOperand))
            {
                throw new FormatException("second operand has wrong format");
            }

            return new CalculatorParameters(operation, firstOperand, secondOperand);
        }

        private bool TryParseOperation(string str, out CalculatorOperation operation)
        {
            switch (str)
            {
                case "-":
                    operation = CalculatorOperation.Diff;
                    return true;
                case "/":
                    operation = CalculatorOperation.Div;
                    return true;
                case "*":
                    operation = CalculatorOperation.Mult;
                    return true;
                case "+":
                    operation = CalculatorOperation.Sum;
                    return true;
            }

            operation = CalculatorOperation.Sum;
            return false;
        }
    }
}
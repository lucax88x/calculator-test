namespace Calculator
{
    public interface IFileWrapper
    {
        string ReadAllText(string path);
    }

    public class FileWrapper : IFileWrapper
    {
        public string ReadAllText(string path)
        {
            return System.IO.File.ReadAllText(path);            
        }
    }
}
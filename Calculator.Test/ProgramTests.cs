﻿using System;
using FluentAssertions;
using NSubstitute;
using Xunit;

namespace Calculator.Test
{
    public class Tests
    {
        private readonly ProgramLogic _sut;
        private readonly IConsoleWrapper _consoleWrapper;
        private readonly IFileWrapper _fileWrapper;

        public Tests()
        {
            _consoleWrapper = Substitute.For<IConsoleWrapper>();
            _fileWrapper = Substitute.For<IFileWrapper>();

            _sut = new ProgramLogic(_consoleWrapper, _fileWrapper);
        }

        [Fact]
        public void should_give_error_message_without_arguments()
        {
            // ACT
            Action run = () => _sut.Run(new string[0]);
            
            // ASSERT
            run.Should().NotThrow<Exception>();
            _consoleWrapper.Received(1).WriteLine("you must provide an argument (interactive or a filepath)");
        }

        [Fact]
        public void when_normal_mode_should_give_error_message_with_empty_content()
        {
            // ARRANGE
            _fileWrapper.ReadAllText("someFilePath").Returns(string.Empty);

            // ACT
            Action run = () => _sut.Run(new[] {"someFilePath"});
            
            // ASSERT
            run.Should().NotThrow<Exception>();
            _consoleWrapper.Received(1).WriteLine("You must provide 2 operands separated by a operation (+, -, /, *)");
        }

        [Fact]
        public void when_normal_mode_should_give_error_message_with_wrong_content_as_operation()
        {
            // ARRANGE
            _fileWrapper.ReadAllText("someFilePath").Returns(string.Join("\r", "3", "unknown_operation", "3"));

            // ACT
            Action run = () => _sut.Run(new[] {"someFilePath"});
            
            // ASSERT
            run.Should().NotThrow<Exception>();
            _consoleWrapper.Received(1).WriteLine("operation has wrong format");
        }
        
        [Fact]
        public void when_normal_mode_should_give_error_message_with_wrong_content_as_first_operand()
        {
            // ARRANGE
            _fileWrapper.ReadAllText("someFilePath").Returns(string.Join("\r", "wrong", "+", "3"));

            // ACT
            Action run = () => _sut.Run(new[] {"someFilePath"});
            
            // ASSERT
            run.Should().NotThrow<Exception>();
            _consoleWrapper.Received(1).WriteLine("first operand has wrong format");
        }

        [Fact]
        public void when_normal_mode_should_give_error_message_with_wrong_content_as_second_operand()
        {
            // ARRANGE
            _fileWrapper.ReadAllText("someFilePath").Returns(string.Join("\r", "3", "+", "wrong"));

            // ACT
            Action run = () => _sut.Run(new[] {"someFilePath"});
            
            // ASSERT
            run.Should().NotThrow<Exception>();
            _consoleWrapper.Received(1).WriteLine("second operand has wrong format");
        }

        [Theory]
        [ClassData(typeof(CalculatorTestData))]
        public void when_interactive_mode_should_compute_correctly(long operator1, string operation, long operator2, long expected)
        {
            // ARRANGE
            _consoleWrapper.ReadLine().Returns(operator1.ToString(), operation, operator2.ToString());

            // ACT
            _sut.Run(new[] {"interactive"});

            // ASSERT            
            _consoleWrapper.Received(1).WriteLine("First argument: ");
            _consoleWrapper.Received(1).WriteLine("Operation (+, -, /, *): ");
            _consoleWrapper.Received(1).WriteLine("Second Argument: ");
            _consoleWrapper.Received(1).WriteLine(expected);
        }

        [Theory]
        [ClassData(typeof(CalculatorTestData))]
        public void when_normal_mode_should_compute_correctly(long operator1, string operation, long operator2, long expected)
        {
            // ARRANGE
            _fileWrapper.ReadAllText("someFilePath").Returns(string.Join("\r", operator1, operation, operator2));

            // ACT
            _sut.Run(new[] {"someFilePath"});

            // ASSERT  
            _consoleWrapper.Received(1).WriteLine(expected);
        }
    }
}
using System.Collections;
using System.Collections.Generic;

namespace Calculator.Test
{
    public class CalculatorTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] {1m, "+", 2m, 3m};
            yield return new object[] {-4m, "+", -6m, -10m};
            yield return new object[] {-2m, "+", 2m, 0m};

            yield return new object[] {1m, "-", 2m, -1m};
            yield return new object[] {-4m, "-", -6m, 2m};
            yield return new object[] {-2m, "-", 2m, -4m};
            yield return new object[] {5m, "-", 2m, 3m};

            yield return new object[] {1m, "/", 2m, 0.5m};
            yield return new object[] {-4m, "/", -6m, 0m};
            yield return new object[] {-2m, "/", 2m, -1m};
            yield return new object[] {5m, "/", 2m, 2.5m};

            yield return new object[] {1m, "*", 2m, 2m};
            yield return new object[] {-4m, "*", -6m, 24m};
            yield return new object[] {-2m, "*", 2m, -4m};
            yield return new object[] {5m, "*", 2m, 10m};
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}